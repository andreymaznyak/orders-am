/**
 * Created by AndreyMaznyak on 11.04.2016.
 */
'use strict';
angular.module('andreymaznyak.orders', ['ngSails', 'andreymaznyak.notifications'])
.service('OrdersManager', function($sails, notificationsService){

        function getFormattedDate(date){
            var year = date.getFullYear();
            var month = (1 + date.getMonth()).toString();
            month = month.length > 1 ? month : '0' + month;
            var day = date.getDate().toString();
            day = day.length > 1 ? day : '0' + day;
            return year + '-' + month  + '-' + day;
        }

        var orders = this.orders = [],
            clients_queue = this.clients_queue = [],
            apiUrl = '/orders',
            events = this.events = {};

        this.updateOrder = updateOrder;

        function updateOrder(order){
            delete order['delivery_date'];
            return $sails.put(apiUrl + '/' + order.id, order).then(receive_orders_success, error);
        }

        this.getOrders = getOrders;

        getOrders();

        function getOrders(){

            check_login();

            return orders;

            function check_login(){
                if(!window.currentUser)
                    setTimeout(check_login,300);
                else
                    get_orders();
            }
        }

        function get_orders(){
            $sails.get(apiUrl + '?delivery_date_str='+ getFormattedDate(new Date())).then(receive_orders_success, error);
            $sails.on('orders', function (resp) {
                switch(resp.verb) {
                    case 'created':
                        $sails.get(apiUrl + '/' + resp.id).then(receive_orders_success,error);
                        break;
                    case 'updated':
                        $sails.get(apiUrl + '/' + resp.id).then(receive_orders_success,error);
                        console.log(resp);
                        break;
                    case 'addedTo':
                        $sails.get(apiUrl + '/' + resp.id).then(receive_orders_success,error);
                        notify_print(resp.id);
                        console.log(resp);
                        break;
                    case 'destroyed':
                        _.remove(orders, {id: resp.id});
                        var client_in_queue = _.find(clients_queue,{ client_number_int: resp.previous.client_number_int });
                        if(!!client_in_queue && client_in_queue.orders.length === 1)
                            clients_queue.splice(clients_queue.indexOf(client_in_queue),1);
                        else{
                            var order = _.find(client_in_queue.orders,{ id: resp.id });
                            if(order){
                                client_in_queue.orders.splice(client_in_queue.orders.indexOf(order),1);
                            }
                            setTimeout(function(){
                                if(!!events.applyScope) {
                                    client_in_queue.orders = _.clone(client_in_queue.orders);
                                    events.applyScope();
                                }
                            },100);
                        }

                        console.log(resp);
                        break;
                    default :{
                        console.log(resp);
                    }
                }
            });
        }

        function receive_orders_success(resp){
            var data = resp.data;

            console.log(data);
            if(!!data.length){
                for(var i = 0; i < data.length; i++){
                    addOrder(data[i]);
                }
            }else if(!!data.id){
                if(data.delivery_date_str === getFormattedDate(new Date()))
                    addOrder(data);
                else
                    return 'delivery date incorrect';
            }
            notify_checked();
            notify_completed();
        }

        function addOrder(data){
            var old_element = _.find(orders,{id:data.id});
            if(!!old_element){
                orders.splice(orders.indexOf(old_element),1);
            }
            data.createdAt = new Date(data.createdAt);
            if(data.delivery_date_str === getFormattedDate(new Date()) && data.status != 'closed'
                && !(!!window.currentUser && window.currentUser.notifyCompleted && !!data.delivery )){
                orders.push(data);
            }
            addToClientsQueue(data);

        }

        function error(err){
            console.log(err);
        }

        function addToClientsQueue( order ){
            var filter_options = {client_number_int: order.client_number_int};
            if(!!window.currentUser && window.currentUser.notifyCompleted && !!order.delivery ){
                return "delivery order don't show in clients queue";
            }
            var client_in_queue = _.find(clients_queue,filter_options);
            if(!client_in_queue && order.status != 'closed'){
                client_in_queue = new Client(order);
                clients_queue.push(client_in_queue);
            }
            if(!!client_in_queue){
                client_in_queue.addOrder(order);
                if(order.status === 'closed'){
                    clients_queue.splice(clients_queue.indexOf(client_in_queue),1);
                }
            }

        }

        function notify_print(id){
            if(window.currentUser.check_orders){
                var printOrder = _.find(orders,{id: id});
                if(!!printOrder){
                    notificationsService.notify({title: '���!!! ����� ������� ������! ' + printOrder.client_number_str});
                    cordova.plugins.notification.local.schedule({
                        id: 1,
                        title: "������� ������ �" + printOrder.client_number_str,
                        sound: "file://sounds/trumpet_alarm_sms_short.mp3",
                        tag: printOrder.client_number_int,
                        text: printOrder.client_number_str,
                        data: { meetingId: printOrder.client_number_int }
                    });
                }
            }
        }

        function notify_checked(){
            if(window.currentUser.notifyChecked){
                notificationsService.notifications.splice(0,notificationsService.notifications.length);
                var checked_clients = _.filter(clients_queue, {status:'checked'});
                for(var i = 0; i < checked_clients.length; i++){
                    var notification = {};
                    notification.title = '������ ������ �' + checked_clients[i].client_number_int;
                    notification.options = {
                        icon: "images/angular.png",
                        body: "",
                        tag:checked_clients[i].client_number_int
                    };
                    notification.options.body += checked_clients[i].client_str + '\n';
                    for(var j = 0; j < checked_clients[i].orders.length; j++)
                        notification.options.body += '�������� �' + checked_clients[i].orders[j].client_number_str + ' ���� �' + checked_clients[i].orders[j].code1c + '\n';
                    notificationsService.notifications.push(notification);
                }
                notificationsService.notifyAll();
            }
        }

        function notify_completed(){
            if(window.currentUser.notifyCompleted && window.device_type === 'nw'){

                notificationsService.notifications.splice(0,notificationsService.notifications.length);
                var checked_clients = _.filter(clients_queue, {status:'completed'});
                for(var i = 0; i < checked_clients.length; i++){
                    var notification = {};
                    notification.title = '�! ��������� ������ � ������� �' + checked_clients[i].client_number_int;
                    notification.options = {
                        icon: "images/angular.png",
                        body: "",
                        tag:checked_clients[i].client_number_int
                    };
                    notification.options.body += checked_clients[i].client_str + '\n';
                    for(var j = 0; j < checked_clients[i].orders.length; j++)
                        notification.options.body += '�������� �' + checked_clients[i].orders[j].client_number_str + ' ���� �' + checked_clients[i].orders[j].code1c + '\n';
                    notificationsService.notifications.push(notification);
                }
                notificationsService.notifyAll();
            }
        }

        function notify_order_queue(checked_client){
            if(window.currentUser.notifyCompleted && window.device_type != 'nw'){

                var notification = {};
                notification.title = '�! ��������� ������ � ������� �' + checked_client.client_number_int;
                notification.options = {
                    icon: "images/angular.png",
                    body: "",
                    tag:checked_client.client_number_int
                };
                notification.options.body += checked_client.client_str + '\n';
                notificationsService.notify(notification);
            }
        }

        function Client(data){
            var self = this;
            this.client_number_int = data.client_number_int;
            this.status = data.status;
            this.orders = [];
            this.delivery_date_str = data.delivery_date_str;
            if(data.client_number_int > 101){
                this.client_str = data.client_str;
            }else{
                this.client_str = '�������';
            }
            this.addOrder = addOrder;

            return this;

            function addOrder(order){
                var order_in_queue = _.find(self.orders,{ id: order.id });
                if(!!order_in_queue){
                    self.orders.splice(self.orders.indexOf(order_in_queue),1);
                }
                if(order.status != 'closed')
                    self.orders.push(order);
                setTimeout(function(){
                    var old_status = self.status;
                    if(check_status('checked')){
                        self.status = 'checked';
                    }else if(check_status('completed')){
                        self.status = 'completed';
                    }else if(check_status('closed')){
                        self.status = 'closed';
                    }else{
                        if(!!_.find(self.orders,{status:'new'})){
                            self.status = 'new';
                        }else if(!!_.find(self.orders,{status:'checked'})){
                            self.status = 'checked';
                        }else if(!!_.find(self.orders,{status:'completed'})){
                            self.status = 'completed';
                        }else{
                            self.status = 'not completed';
                        }
                    }
                    if((old_status === 'checked' || old_status === 'new') && self.status === 'completed'){
                        notify_order_queue(self);
                    }
                    if(!!events.applyScope){
                        self.orders = _.clone(self.orders);
                        events.applyScope();
                    }
                }, 300);

            }

            function check_status(status){
                if(self.orders.length > 0){
                    for(var i = 0; i < self.orders.length; i++){
                        if(self.orders[i].status != status){
                            return false;
                        }
                    }
                }
                return true;
            }

        }
});